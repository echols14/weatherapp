package ss.virovitica.weatherapp

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.realm.Realm
import ss.virovitica.weatherapp.di.AppComponent
import ss.virovitica.weatherapp.di.AppModule
import ss.virovitica.weatherapp.di.DaggerAppComponent
import javax.inject.Inject

class WeatherApp: Application(), HasActivityInjector {
    companion object{
        private lateinit var appComponent: AppComponent
        lateinit var instance: WeatherApp
            private set
    }
    private lateinit var dispatchingAndroidActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    fun inject(dispatchingAndroidActivityInjectorIn: DispatchingAndroidInjector<Activity>) {
        dispatchingAndroidActivityInjector = dispatchingAndroidActivityInjectorIn
    }

    override fun onCreate() {
        super.onCreate()
        instance = this //singleton
        //dagger
        appComponent = buildComponent()
        appComponent.inject(this)
        //initialize Realm
        Realm.init(instance)
    }

    private fun buildComponent(): AppComponent =
        DaggerAppComponent.builder()
            .appModule(AppModule(instance))
            .build()

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidActivityInjector
}