package ss.virovitica.weatherapp.activity.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.TextView
import com.bumptech.glide.Glide
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.find
import org.jetbrains.anko.longToast
import ss.virovitica.weatherapp.R
import ss.virovitica.weatherapp.domain.model.Forecast
import ss.virovitica.weatherapp.utility.ToolbarManager
import ss.virovitica.weatherapp.utility.color
import ss.virovitica.weatherapp.utility.textColor
import javax.inject.Inject

class DetailActivity: AppCompatActivity(), IDetailView, ToolbarManager {
    companion object {
        const val ID_KEY = "DetailActivity:id"
        const val CITY_NAME_KEY = "DetailActivity:cityName"
        fun newIntent(context: Context, id: String, cityName: String?): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(ID_KEY, id)
            intent.putExtra(CITY_NAME_KEY, cityName)
            return intent
        }
    }
    private lateinit var presenter: IDetailPresenter
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }

    @Inject
    fun inject(presenterIn: IDetailPresenter){
        presenter = presenterIn
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        initToolbar()
        toolbarTitle = intent.getStringExtra(CITY_NAME_KEY)
        enableHomeAsUp { onBackPressed() }
        presenter.getForecastById(intent.getStringExtra(ID_KEY))
    }

    override fun setForecast(forecast: Forecast) = with(forecast){
        Glide.with(ctx).load(forecast.iconUrl).into(icon)
        supportActionBar?.subtitle = date
        weatherDescription.text = description
        bindWeather(high to maxTemperature, low to minTemperature, humidity to humidityText)
        humidityText.textColor = color(android.R.color.holo_blue_light)
    }

    private fun bindWeather(vararg views: Pair<Int, TextView>) = views.forEach {
        it.second.text = it.first.toString()
        it.second.textColor = color(when (it.first) {
            in -2000..5 -> android.R.color.holo_blue_dark
            in 5..15 -> android.R.color.holo_green_dark
            in 10..25 -> android.R.color.holo_orange_dark
            else ->android.R.color.holo_red_dark
        })
    }

    override fun toast(message: String){
        longToast(message)
    }

    fun getForecastId(): String = presenter.getForecastID()
}