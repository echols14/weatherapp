package ss.virovitica.weatherapp.activity.detail

import ss.virovitica.weatherapp.domain.model.Forecast
import ss.virovitica.weatherapp.domain.model.requestForecast
import ss.virovitica.weatherapp.utility.Listener
import javax.inject.Inject

class DetailInteractor @Inject constructor(): IDetailInteractor {
    override fun getForecastById(id: String, listener: Listener<Forecast>) {
        val result: Forecast? = requestForecast(id)
        if (result == null) listener.onError()
        else listener.onSuccess(result)
    }
}