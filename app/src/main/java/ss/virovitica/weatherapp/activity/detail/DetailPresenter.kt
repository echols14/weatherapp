package ss.virovitica.weatherapp.activity.detail

import ss.virovitica.weatherapp.domain.model.Forecast
import ss.virovitica.weatherapp.utility.Listener
import javax.inject.Inject

class DetailPresenter @Inject constructor(private val view: IDetailView, private val interactor: IDetailInteractor): IDetailPresenter, Listener<Forecast> {
    private lateinit var forecastId: String
    override fun getForecastById(id: String) {
        interactor.getForecastById(id, this)
    }

    override fun onSuccess(item: Forecast) {
        forecastId = item.id
        view.setForecast(item)
    }

    override fun onError() {
        view.toast("Failed to load the forecast")
    }

    override fun getForecastID(): String = forecastId
}