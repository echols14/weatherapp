package ss.virovitica.weatherapp.activity.detail

import ss.virovitica.weatherapp.domain.model.Forecast
import ss.virovitica.weatherapp.utility.Listener

interface IDetailInteractor {
    fun getForecastById(id: String, listener: Listener<Forecast>)
}