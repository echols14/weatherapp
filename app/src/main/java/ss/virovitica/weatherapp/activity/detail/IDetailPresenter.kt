package ss.virovitica.weatherapp.activity.detail

interface IDetailPresenter {
    fun getForecastById(id: String)
    fun getForecastID(): String
}