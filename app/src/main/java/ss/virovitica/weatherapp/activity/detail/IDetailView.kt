package ss.virovitica.weatherapp.activity.detail

import ss.virovitica.weatherapp.domain.model.Forecast

interface IDetailView {
    fun setForecast(forecast: Forecast)
    fun toast(message: String)
}