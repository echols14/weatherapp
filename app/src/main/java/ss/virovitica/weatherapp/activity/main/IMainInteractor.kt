package ss.virovitica.weatherapp.activity.main

import ss.virovitica.weatherapp.domain.model.Forecast
import ss.virovitica.weatherapp.domain.model.ForecastList
import ss.virovitica.weatherapp.utility.Interactor
import ss.virovitica.weatherapp.utility.Listener

interface IMainInteractor: Interactor{
    fun getWeather(zipCode: String, listener: Listener<ForecastList>)
    fun getWeather(listener: Listener<ForecastList>)
    fun getForecast(id: String): Forecast?
}