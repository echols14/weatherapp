package ss.virovitica.weatherapp.activity.main

import ss.virovitica.weatherapp.domain.model.ForecastList
import ss.virovitica.weatherapp.utility.Presenter

interface IMainPresenter: Presenter {
    var zipCode: Long
    var usesLatLon: Long
    fun getWeather()
    fun setWeather(forecastList: ForecastList)
}