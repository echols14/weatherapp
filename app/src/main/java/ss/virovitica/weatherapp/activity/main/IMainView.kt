package ss.virovitica.weatherapp.activity.main

import ss.virovitica.weatherapp.domain.model.ForecastList
import ss.virovitica.weatherapp.utility.IView

interface IMainView: IView {
    fun getWeather()
    fun setWeather(weekForecast: ForecastList)
    fun toast(message: String)
}