package ss.virovitica.weatherapp.activity.main

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.find
import org.jetbrains.anko.longToast
import ss.virovitica.weatherapp.R
import ss.virovitica.weatherapp.adapter.ForecastListAdapter
import ss.virovitica.weatherapp.domain.model.ForecastList
import ss.virovitica.weatherapp.utility.ToolbarManager
import javax.inject.Inject
import android.support.v7.widget.DividerItemDecoration

class MainActivity: AppCompatActivity(), IMainView, ToolbarManager { //TODO: chapter 26
    override val ctx: Context
        get() = this
    private lateinit var presenter: IMainPresenter
    private lateinit var adapter: ForecastListAdapter
    override val toolbar by lazy { find<Toolbar>(R.id.toolbar) }

    @Inject
    fun inject(presenterIn: IMainPresenter, adapterIn: ForecastListAdapter){
        presenter = presenterIn
        adapter = adapterIn
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()
        //set up the recycler view
        forecastList.layoutManager = LinearLayoutManager(this)
        forecastList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        attachToScroll(forecastList)
        forecastList.adapter = adapter
        //get weather (async)
        getWeather()
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun getWeather() = presenter.getWeather()

    override fun setWeather(weekForecast: ForecastList){ //callback from getWeather
        toolbarTitle = if(weekForecast.country == "") "(${getString(R.string.no_country)})"
                        else "${weekForecast.city?.name} (${weekForecast.country})"
        adapter.cityName = weekForecast.city?.name
        adapter.weekForecast = weekForecast
        adapter.notifyDataSetChanged()
    }

    override fun toast(message: String) {
        longToast(message)
    }
}
