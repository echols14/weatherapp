package ss.virovitica.weatherapp.activity.main

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import ss.virovitica.weatherapp.domain.mapper.ForecastDataMapper
import ss.virovitica.weatherapp.domain.model.*
import ss.virovitica.weatherapp.net.ForecastApiClient
import ss.virovitica.weatherapp.net.ForecastResult
import ss.virovitica.weatherapp.utility.Listener
import javax.inject.Inject

class MainInteractor @Inject constructor(private val forecastApiClient: ForecastApiClient): IMainInteractor {
    private var disposable: CompositeDisposable? = null
    override fun getWeather(zipCode: String, listener: Listener<ForecastList>) {
        setObserver(forecastApiClient.requestForecast(zipCode), listener)
    }

    override fun getWeather(listener: Listener<ForecastList>) {
        val location = requestLatLon()
        if(location == null) listener.onError()
        else setObserver(forecastApiClient.requestForecast(location.lat, location.lon), listener)
    }

    private fun setObserver(observable: Observable<ForecastResult>, listener: Listener<ForecastList>) {
        disposable = CompositeDisposable()
        disposable?.add(observable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object: DisposableObserver<ForecastResult>(){
                override fun onNext(response: ForecastResult) {
                    val forecastList = ForecastDataMapper().convertFromDataModel(response)
                    setForecast(forecastList)
                    listener.onSuccess(forecastList)
                }
                override fun onError(e: Throwable) = listener.onError()
                override fun onComplete() {}
            })
        )
    }

    override fun getForecast(id: String): Forecast? {
        return requestForecast(id)
    }

    override fun unsubscribe() {
        if(disposable?.isDisposed == false) disposable?.dispose()
    }
}