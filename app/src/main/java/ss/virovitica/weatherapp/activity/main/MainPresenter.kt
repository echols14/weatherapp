package ss.virovitica.weatherapp.activity.main

import ss.virovitica.weatherapp.activity.settings.SettingsPresenter
import ss.virovitica.weatherapp.domain.model.ForecastList
import ss.virovitica.weatherapp.utility.DelegatesExt
import ss.virovitica.weatherapp.utility.Listener
import javax.inject.Inject

class MainPresenter @Inject constructor(private val view: IMainView, private val interactor: IMainInteractor): IMainPresenter, Listener<ForecastList> {
    override var zipCode: Long by DelegatesExt.longPreference(view.ctx, SettingsPresenter.ZIP_CODE, SettingsPresenter.DEFAULT_ZIP)
    override var usesLatLon: Long by DelegatesExt.longPreference(view.ctx, SettingsPresenter.LAT_LON_BOOL, SettingsPresenter.DEFAULT_LAT_LON)

    override fun getWeather(){
        if(usesLatLon == 0L) interactor.getWeather(zipCode.toString(), this as Listener<ForecastList>)
        else interactor.getWeather(this as Listener<ForecastList>)
    }

    override fun setWeather(forecastList: ForecastList) {
        view.setWeather(forecastList)
    }

    override fun onSuccess(item: ForecastList) {
        setWeather(item)
    }

    override fun onError() {
        view.toast("Failed to load the forecast")
    }

    override fun unsubscribe() {
        interactor.unsubscribe()
    }
}