package ss.virovitica.weatherapp.activity.settings

import ss.virovitica.weatherapp.domain.model.Location
import ss.virovitica.weatherapp.utility.Interactor
import ss.virovitica.weatherapp.utility.Listener

interface ISettingsInteractor: Interactor {
    fun reverseGeocode(latitude: Double, longitude: Double, listener: Listener<Long>)
    fun getLatLon(): Location?
}