package ss.virovitica.weatherapp.activity.settings

import ss.virovitica.weatherapp.domain.model.Location
import ss.virovitica.weatherapp.utility.Presenter

interface ISettingsPresenter: Presenter {
    var zipCode: Long
    var usesLatLon: Long //acts as a boolean with 0L and 1L
    fun reverseGeocode(latitude: Double, longitude: Double)
    fun getLatLon(): Location?
}