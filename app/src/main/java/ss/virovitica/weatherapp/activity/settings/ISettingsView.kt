package ss.virovitica.weatherapp.activity.settings

import ss.virovitica.weatherapp.utility.IView

interface ISettingsView: IView {
    var justClicked: Boolean
    fun setZipText(zipCode: String, showWindow: Boolean)
}