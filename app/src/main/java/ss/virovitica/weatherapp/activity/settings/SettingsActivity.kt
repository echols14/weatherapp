package ss.virovitica.weatherapp.activity.settings

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.*
import ss.virovitica.weatherapp.R
import ss.virovitica.weatherapp.activity.main.MainActivity
import javax.inject.Inject

class SettingsActivity: AppCompatActivity(), ISettingsView, OnMapReadyCallback {
    companion object {
        private const val ZOOM_FOCUS = 5f
    }
    @Inject lateinit var presenter: ISettingsPresenter
    override val ctx: Context
        get() = this
    private var zipString = ""
    private var map: GoogleMap? = null
    private var marker: Marker? = null
    override var justClicked: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //set the zip code EditText
        zipString = presenter.zipCode.toString()
        cityCode.setText(zipString)
        cityCode.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if(!justClicked) presenter.usesLatLon = 0L
                zipString = s.toString()
                goButton.isEnabled = (zipString != "")
            }
        })
        //make the button do stuff
        goButton.setOnClickListener {
            if(zipString.toLongOrNull() != null) presenter.zipCode = zipString.toLong()
            toast(R.string.location_saved)
            startActivity(intentFor<MainActivity>().clearTop())
        }
        //load the map
        val map: MapView = findViewById(R.id.mapView)
        map.onCreate(null)
        map.onResume()
        map.getMapAsync(this)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> false
    }

    override fun setZipText(zipCode: String, showWindow: Boolean) {
        zipString = zipCode
        cityCode.setText(zipCode)
        marker?.title = zipCode
        if(showWindow) marker?.showInfoWindow()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        if(presenter.usesLatLon == 1L) {
            val latLon = presenter.getLatLon()
            if(latLon != null) {
                val location = LatLng(latLon.lat, latLon.lon)
                map?.animateCamera(CameraUpdateFactory.newLatLngZoom(location, ZOOM_FOCUS))
                marker = map?.addMarker(MarkerOptions().position(location))
            }
        }
        map?.setOnMapClickListener {
            marker?.remove()
            marker = map?.addMarker(MarkerOptions().position(LatLng(it.latitude, it.longitude)))
            justClicked = true
            presenter.usesLatLon = 1L
            presenter.reverseGeocode(it.latitude, it.longitude)
        }
    }
}