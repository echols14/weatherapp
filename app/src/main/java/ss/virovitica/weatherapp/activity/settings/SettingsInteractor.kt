package ss.virovitica.weatherapp.activity.settings

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import ss.virovitica.weatherapp.domain.model.Location
import ss.virovitica.weatherapp.domain.model.requestLatLon
import ss.virovitica.weatherapp.domain.model.setLatLong
import ss.virovitica.weatherapp.net.GeocodeApiClient
import ss.virovitica.weatherapp.net.ReverseGeocodeResponse
import ss.virovitica.weatherapp.utility.Listener
import javax.inject.Inject

class SettingsInteractor @Inject constructor(private val geocodeApiClient: GeocodeApiClient): ISettingsInteractor {
    companion object{
        private fun extractPostalCode(response: ReverseGeocodeResponse): String?{
            val view = response.Response.View
            if(view.isEmpty()) return null
            val result = view[0].Result
            if(result.isEmpty()) return null
            return result[0].Location.Address.PostalCode
        }
    }
    private var disposable: CompositeDisposable? = null
    override fun reverseGeocode(latitude: Double, longitude: Double, listener: Listener<Long>) {
        setLatLong(latitude, longitude)
        disposable = CompositeDisposable()
        disposable?.add(geocodeApiClient.reverseGeocode(latitude, longitude)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object: DisposableObserver<ReverseGeocodeResponse>(){
                override fun onNext(response: ReverseGeocodeResponse) {
                    val postalCode = extractPostalCode(response)
                    if(postalCode != null && postalCode.toLongOrNull() != null) listener.onSuccess(postalCode.toLong())
                    else listener.onError()
                }
                override fun onError(e: Throwable) = listener.onError()
                override fun onComplete() {}
            })
        )
    }

    override fun getLatLon(): Location? = requestLatLon()

    override fun unsubscribe() {
        if(disposable?.isDisposed == false) disposable?.dispose()
    }
}