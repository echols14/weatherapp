package ss.virovitica.weatherapp.activity.settings

import org.jetbrains.anko.longToast
import ss.virovitica.weatherapp.R
import ss.virovitica.weatherapp.utility.DelegatesExt
import ss.virovitica.weatherapp.utility.Listener
import javax.inject.Inject

class SettingsPresenter @Inject constructor(private val view: ISettingsView, private val interactor: ISettingsInteractor): ISettingsPresenter, Listener<Long> {
    companion object {
        const val ZIP_CODE = "zipCode"
        const val DEFAULT_ZIP = 94043L
        const val LAT_LON_BOOL = "latLonBoolean"
        const val DEFAULT_LAT_LON = 0L
    }
    override var zipCode: Long by DelegatesExt.longPreference(view.ctx, ZIP_CODE, DEFAULT_ZIP)
    override var usesLatLon: Long by DelegatesExt.longPreference(view.ctx, LAT_LON_BOOL, DEFAULT_LAT_LON)

    override fun reverseGeocode(latitude: Double, longitude: Double){
        interactor.reverseGeocode(latitude, longitude, this)
    }

    override fun onSuccess(item: Long) {
        zipCode = item
        view.setZipText(zipCode.toString(), true)
    }

    override fun onError() {
        view.setZipText(view.ctx.getString(R.string.unknown_zip), false)
        view.justClicked = false
        view.ctx.longToast(R.string.zipCode_not_found)
    }

    override fun unsubscribe() {
        interactor.unsubscribe()
    }

    override fun getLatLon() = if(usesLatLon == 1L) interactor.getLatLon() else null
}