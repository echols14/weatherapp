package ss.virovitica.weatherapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_forecast.view.*
import ss.virovitica.weatherapp.R
import ss.virovitica.weatherapp.activity.detail.DetailActivity
import ss.virovitica.weatherapp.domain.model.Forecast
import ss.virovitica.weatherapp.domain.model.ForecastList
import ss.virovitica.weatherapp.utility.ctx

class ForecastListAdapter: RecyclerView.Adapter<ForecastListAdapter.ViewHolder>(){
    var weekForecast: ForecastList = ForecastList()
    var cityName: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.ctx).inflate(R.layout.item_forecast, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)  {
        holder.bindForecast(weekForecast[position], cityName)
    }

    override fun getItemCount(): Int = weekForecast.size

    class ViewHolder(private val view: View): RecyclerView.ViewHolder(view), View.OnClickListener {
        private lateinit var context: Context
        private lateinit var forecastID: String
        private val listener: View.OnClickListener = this
        private var cityName: String? = null

        fun bindForecast(forecast: Forecast?, cityName: String?){
            this.cityName = cityName
            context = itemView.context
            forecastID = forecast?.id ?: "UNINITIALIZED"
            if(forecast != null) {
                with(forecast) {
                    Glide.with(view).load(iconUrl).into(itemView.icon)
                    itemView.date.text = date
                    itemView.description.text = description
                    itemView.maxTemperature.text = "$high"
                    itemView.minTemperature.text = "$low"
                    itemView.setOnClickListener(listener)
                }
            }
        }

        override fun onClick(v: View?) {
            val intent = DetailActivity.newIntent(context, forecastID, cityName)
            context.startActivity(intent)
        }
    }
}