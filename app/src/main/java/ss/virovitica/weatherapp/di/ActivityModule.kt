package ss.virovitica.weatherapp.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ss.virovitica.weatherapp.activity.detail.DetailActivity
import ss.virovitica.weatherapp.activity.main.MainActivity
import ss.virovitica.weatherapp.activity.settings.SettingsActivity

@Module
abstract class ActivityModule {
    @PerActivity
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [DetailModule::class])
    abstract fun contributeDetailActivity(): DetailActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [SettingsModule::class])
    abstract fun contributeSettingsActivity(): SettingsActivity
}