package ss.virovitica.weatherapp.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import ss.virovitica.weatherapp.WeatherApp
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class, AndroidInjectionModule::class, ActivityModule::class])
interface AppComponent {
    fun inject(app: WeatherApp)
}