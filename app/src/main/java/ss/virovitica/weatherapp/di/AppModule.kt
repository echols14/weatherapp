package ss.virovitica.weatherapp.di

import dagger.Module
import dagger.Provides
import ss.virovitica.weatherapp.WeatherApp
import ss.virovitica.weatherapp.domain.model.Forecast
import javax.inject.Singleton

@Module
class AppModule(private val app: WeatherApp) {
    @Provides @Singleton
    fun provideWeatherApp() = app
    @Provides @Singleton
    fun provideMainModule() = MainModule()
    @Provides
    fun provideDefaultForecast() = Forecast()
}