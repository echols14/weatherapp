package ss.virovitica.weatherapp.di

import dagger.Module
import dagger.Provides
import ss.virovitica.weatherapp.activity.detail.*

@Module
class DetailModule {
    @Provides @PerActivity
    fun provideDetailView(activity: DetailActivity): IDetailView = activity
    @Provides @PerActivity
    fun provideDetailPresenter(presenter: DetailPresenter): IDetailPresenter = presenter
    @Provides @PerActivity
    fun provideDetailInteractor(interactor: DetailInteractor): IDetailInteractor = interactor
}