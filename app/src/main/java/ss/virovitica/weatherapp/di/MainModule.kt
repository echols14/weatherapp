package ss.virovitica.weatherapp.di

import dagger.Module
import dagger.Provides
import org.jetbrains.anko.toast
import ss.virovitica.weatherapp.activity.main.*
import ss.virovitica.weatherapp.adapter.ForecastListAdapter

@Module
class MainModule {
    @Provides @PerActivity
    fun provideMainView(activity: MainActivity): IMainView = activity
    @Provides @PerActivity
    fun provideMainPresenter(presenter: MainPresenter): IMainPresenter = presenter
    @Provides @PerActivity
    fun provideMainInteractor(interactor: MainInteractor): IMainInteractor = interactor
    @Provides @PerActivity
    fun provideAdapter() = ForecastListAdapter()
}