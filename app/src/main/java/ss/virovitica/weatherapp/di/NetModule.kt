package ss.virovitica.weatherapp.di

import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ss.virovitica.weatherapp.net.ForecastApiClient
import ss.virovitica.weatherapp.net.GeocodeApiClient
import javax.inject.Named

@Module
class NetModule {
    companion object {
        private const val FORECAST_OK_CLIENT = "forecastOkClient"
        const val FORECAST_RETROFIT = "forecastRetrofit"
        private const val GEOCODING_OK_CLIENT = "geocodingOkClient"
        const val GEOCODING_RETROFIT = "geocodingRetrofit"
    }

    @Provides @Singleton
    internal fun provideInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides @Singleton @Named(FORECAST_OK_CLIENT)
    internal fun provideForecastOkClient(interceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(interceptor)
            .readTimeout(ForecastApiClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(ForecastApiClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }

    @Provides @Singleton @Named(FORECAST_RETROFIT)
    internal fun provideForecastRetrofit(@Named(FORECAST_OK_CLIENT) okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ForecastApiClient.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build() //uses Gson to interpret the response
    }

    @Provides @Singleton @Named(GEOCODING_OK_CLIENT)
    internal fun provideGeocodingOkClient(interceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(interceptor)
            .readTimeout(GeocodeApiClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(GeocodeApiClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }

    @Provides @Singleton @Named(GEOCODING_RETROFIT)
    internal fun provideGeocodingRetrofit(@Named(GEOCODING_OK_CLIENT) okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(GeocodeApiClient.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build() //uses Gson to interpret the response
    }
}
