package ss.virovitica.weatherapp.di

import javax.inject.Scope
import kotlin.annotation.Retention

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class PerActivity