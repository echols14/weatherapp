package ss.virovitica.weatherapp.di

import dagger.Module
import dagger.Provides
import ss.virovitica.weatherapp.activity.settings.*

@Module
class SettingsModule {
    @Provides @PerActivity
    fun provideSettingsView(activity: SettingsActivity): ISettingsView = activity
    @Provides @PerActivity
    fun provideSettingsPresenter(presenter: SettingsPresenter): ISettingsPresenter = presenter
    @Provides @PerActivity
    fun provideSettingsInteractor(interactor: SettingsInteractor): ISettingsInteractor = interactor
}