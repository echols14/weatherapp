package ss.virovitica.weatherapp.domain.mapper

import io.realm.RealmList
import ss.virovitica.weatherapp.net.Forecast
import ss.virovitica.weatherapp.domain.model.ForecastList
import ss.virovitica.weatherapp.net.ForecastResult
import java.text.DateFormat
import ss.virovitica.weatherapp.domain.model.Forecast as ModelForecast
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class ForecastDataMapper {
    fun convertFromDataModel(forecast: ForecastResult): ForecastList {
        return ForecastList(
            forecast.city,
            forecast.city.country,
            convertForecastListToDomain(forecast.list)
        )
    }
    private fun convertForecastListToDomain(list: List<Forecast>): RealmList<ModelForecast>{
        val tempList: List<ModelForecast> = list.mapIndexed{ i, forecast ->
            val dt = Calendar.getInstance().timeInMillis + TimeUnit.DAYS.toMillis(i.toLong())
            convertForecastItemToDomain(forecast.copy(dt = dt))
        }
        val realmList = RealmList<ModelForecast>()
        tempList.forEach {realmList.add(it)}
        return realmList
    }
    private fun convertForecastItemToDomain(forecast: Forecast): ModelForecast{
        return ModelForecast(UUID.randomUUID().toString(), convertDate(forecast.dt), forecast.weather[0].description,
            forecast.temp.max.toInt(), forecast.temp.min.toInt(), forecast.humidity, generateIconUrl(forecast.weather[0].icon))
    }
    private fun generateIconUrl(iconCode: String): String = "http://openweathermap.org/img/w/$iconCode.png"
    private fun convertDate(date: Long): String {
        val df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault())
        val sdf = SimpleDateFormat("EEEE", Locale.getDefault())
        return sdf.format(date) + ", " + df.format(date)
    }
}