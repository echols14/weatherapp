package ss.virovitica.weatherapp.domain.model

import io.realm.RealmList
import io.realm.RealmObject
import ss.virovitica.weatherapp.net.City

open class ForecastList(var city: City? = City(), var country: String = "UNINITIALIZED",
                        var dailyForecast: RealmList<Forecast> = RealmList()): RealmObject() {
    val size: Int
        get() = dailyForecast.size
    operator fun get(position: Int) = dailyForecast[position]
}

open class Forecast(var id: String = "UNINITIALIZED", var date: String = "UNINITIALIZED", var description: String = "UNINITIALIZED",
                    var high: Int = 0, var low: Int = 0, var humidity: Int = 0, var iconUrl: String = "UNINITIALIZED"): RealmObject()

open class Location(var lat: Double = 0.0, var lon: Double = 0.0): RealmObject()