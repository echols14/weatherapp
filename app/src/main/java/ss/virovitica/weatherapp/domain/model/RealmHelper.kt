package ss.virovitica.weatherapp.domain.model

import io.realm.Realm

fun requestForecast(id: String): Forecast? {
    val realm = Realm.getDefaultInstance()
    val answer = realm.where(Forecast::class.java).equalTo("id", id).findFirst()
    val toReturn = if (answer != null) realm.copyFromRealm(answer) else null
    realm.close()
    return toReturn
}

fun setForecast(forecast: ForecastList){
    val realm = Realm.getDefaultInstance()
    realm.beginTransaction()
    realm.delete(ForecastList::class.java)
    realm.copyToRealm(forecast)
    realm.commitTransaction()
    realm.close()
}

fun requestLatLon(): Location? {
    val realm = Realm.getDefaultInstance()
    val answer = realm.where(Location::class.java).findFirst()
    val toReturn = if (answer != null) realm.copyFromRealm(answer) else null
    realm.close()
    return toReturn
}

fun setLatLong(latitude: Double, longitude: Double){
    val realm = Realm.getDefaultInstance()
    realm.beginTransaction()
    realm.delete(Location::class.java)
    realm.copyToRealm(Location(latitude, longitude))
    realm.commitTransaction()
    realm.close()
}