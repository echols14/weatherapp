package ss.virovitica.weatherapp.net

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import ss.virovitica.weatherapp.di.NetModule
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class ForecastApiClient @Inject constructor(@Named(NetModule.FORECAST_RETROFIT) private val retrofit: Retrofit) {
    companion object {
        const val TIMEOUT_SECONDS: Long = 10
        const val BASE_URL = "http://api.openweathermap.org/"
        private const val PATH = "data/2.5/forecast/daily"

        private const val MODE = "mode"
        private const val UNITS = "units"
        private const val CNT = "cnt"
        private const val APP_ID = "APPID"
        private const val Q = "q"
        private const val LAT = "lat"
        private const val LON = "lon"

        private const val MODE_VAL = "json"
        private const val UNITS_VAL = "metric"
        private const val CNT_VAL = "7"
        private const val APP_ID_VAL = "15646a06818f61f7b8d7823ca833e1ce"
    }

    fun requestForecast(zipCode: String): Observable<ForecastResult> {
        return retrofit.create(ForecastClient::class.java).getForecast(MODE_VAL, UNITS_VAL, CNT_VAL, APP_ID_VAL, zipCode)
    }
    fun requestForecast(latitude: Double, longitude: Double): Observable<ForecastResult> {
        return retrofit.create(ForecastClient::class.java).getForecast(MODE_VAL, UNITS_VAL, CNT_VAL, APP_ID_VAL, latitude.toString(), longitude.toString())
    }

    private interface ForecastClient{
        @GET(PATH)
        fun getForecast(@Query(MODE) mode: String, @Query(UNITS) units: String, @Query(CNT) cnt: String, @Query(APP_ID) appId: String, @Query(Q) zip: String): Observable<ForecastResult>
        @GET(PATH)
        fun getForecast(@Query(MODE) mode: String, @Query(UNITS) units: String, @Query(CNT) cnt: String, @Query(APP_ID) appId: String, @Query(LAT) lat: String, @Query(LON) lon: String): Observable<ForecastResult>
    }
}