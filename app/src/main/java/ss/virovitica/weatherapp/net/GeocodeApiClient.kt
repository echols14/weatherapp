package ss.virovitica.weatherapp.net

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import ss.virovitica.weatherapp.di.NetModule
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class GeocodeApiClient @Inject constructor(@Named(NetModule.GEOCODING_RETROFIT) private val retrofit: Retrofit) {
    companion object{
        const val TIMEOUT_SECONDS: Long = 10
        const val BASE_URL = "https://reverse.geocoder.api.here.com/"
        private const val PATH = "6.2/reversegeocode.json"

        private const val PROX = "prox"
        private const val MODE = "mode"
        private const val MAX_RESULTS = "maxresults"
        private const val GEN = "gen"
        private const val APP_ID = "app_id"
        private const val APP_CODE = "app_code"

        private const val C = ","
        private const val RADIUS = "250"
        private const val MODE_VAL = "retrieveAddresses"
        private const val MAX_RESULTS_VAL = "1"
        private const val GEN_VAL = "9"
        private const val APP_ID_VAL = "o44vSe7QdmWFDrs06a02"
        private const val APP_CODE_VAL = "1YE95C5tqQlZTOHG8gW3WA"
    }

    fun reverseGeocode(latitude: Double, longitude: Double): Observable<ReverseGeocodeResponse> {
        val proxVal = latitude.toString() + C + longitude.toString() + C + RADIUS
        return retrofit.create(GeocodeClient::class.java).getZipcode(proxVal, MODE_VAL, MAX_RESULTS_VAL, GEN_VAL, APP_ID_VAL, APP_CODE_VAL)
    }

    private interface GeocodeClient{
        @GET(PATH)
        fun getZipcode(@Query(PROX) prox: String, @Query(MODE) mode: String, @Query(MAX_RESULTS) max: String,
                       @Query(GEN) gen: String, @Query(APP_ID) id: String, @Query(APP_CODE) code: String): Observable<ReverseGeocodeResponse>
    }
}