package ss.virovitica.weatherapp.net

import io.realm.RealmObject

data class ForecastResult(val city: City, val list: List<Forecast>)
open class City(var id: Long = -1, var name: String = "UNINITIALIZED", var coord: Coordinates? = Coordinates(),
                var country: String = "UNINITIALIZED", var population: Int = -1): RealmObject()
open class Coordinates(var lon: Float = 0F, var lat: Float = 0F): RealmObject()
data class Forecast(val dt: Long, val temp: Temperature, val pressure: Float, val humidity: Int, val weather: List<Weather>,
                    val speed: Float, val deg: Int, val clouds: Int, val rain: Float)
data class Temperature(val day: Float, val min: Float, val max: Float, val night: Float, val eve: Float, val morn: Float)
data class Weather(val id: Long, val main: String, val description: String, val icon: String)