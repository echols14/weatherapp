package ss.virovitica.weatherapp.net

data class ReverseGeocodeResponse(val Response: Response)
data class Response(val View: Array<ViewBox>)
data class ViewBox(val Result: Array<ResultBox>)
data class ResultBox(val Location: Location)
data class Location(val Address: Address)
data class Address(val PostalCode: String?)