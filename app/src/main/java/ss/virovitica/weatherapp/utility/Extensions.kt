package ss.virovitica.weatherapp.utility

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import java.text.DateFormat
import java.util.*
import kotlin.reflect.KProperty

val View.ctx: Context
    get() = context

fun Long.toDateString(dateFormat: Int = DateFormat.MEDIUM): String {
    val df = DateFormat.getDateInstance(dateFormat, Locale.getDefault())
    return df.format(this)
}

fun Context.color(res: Int): Int = ContextCompat.getColor(this, res)

var TextView.textColor: Int
    get() = currentTextColor
    set(v) = setTextColor(v)

fun View.slideExit() {
    if (translationY == 0f) animate().translationY(-height.toFloat())
}
fun View.slideEnter() {
    if (translationY < 0f) animate().translationY(0f)
}

class LongPreference(val context: Context, val name: String, val default: Long){

    val prefs by lazy {
        context.getSharedPreferences("default", Context.MODE_PRIVATE)
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): Long {
        return prefs.getLong(name, default)
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long) {
        prefs.edit().putLong(name, value).apply()
    }
}

object DelegatesExt {
    fun longPreference(context: Context, name: String, default: Long) = LongPreference(context, name, default)
}