package ss.virovitica.weatherapp.utility

import android.content.Context

interface IView {
    val ctx: Context
}