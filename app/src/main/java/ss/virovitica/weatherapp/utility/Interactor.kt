package ss.virovitica.weatherapp.utility

interface Interactor {
    fun unsubscribe()
}