package ss.virovitica.weatherapp.utility

interface Listener<T> {
    fun onSuccess(item: T)
    fun onError()
}