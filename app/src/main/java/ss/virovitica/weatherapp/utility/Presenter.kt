package ss.virovitica.weatherapp.utility

interface Presenter {
    fun unsubscribe()
}